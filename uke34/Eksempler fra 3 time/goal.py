def goal_results(hjemme, borte):
    if hjemme > borte:
        return 'hjemmeseier'
    elif hjemme == borte:
        return "uavgjort"
    else:
        return "borteseier"
    
print(goal_results(2,3))
print(goal_results(4,3))
print(goal_results(2,2))