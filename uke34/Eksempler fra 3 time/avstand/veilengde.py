'''
Dette er utenfor pensum, men kun tenkt å vise hva en kan få til med 
Python og noen gode ekstra bibliotek/moduler, med få linjer
'''
import simplejson, urllib, requests
from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent="foo")
location1 = geolocator.geocode(input("Sted 1: "))
location2 = geolocator.geocode(input("Sted 2: "))

print(f"La oss finne avstanden mellom {location1.address.split(',')[0]} og \
{location2.address.split(',')[0]}.")

orig_coord = f"{location1.longitude},{location1.latitude};"
dest_coord = f"{location2.longitude},{location2.latitude}"

# https://stackoverflow.com/questions/48221046/osrm-giving-wrong-response-for-distance-between-2-points
url =  'http://router.project-osrm.org/route/v1/driving/'+orig_coord+dest_coord
payload = {"steps":"true","geometries":"geojson"}
response = requests.get(url,params=payload)
data = response.json()
km = data['routes'][0]['legs'][0]['distance']/1000
print(f"Veilengden mellom {location1} og {location2} er {km:.2f} km.")