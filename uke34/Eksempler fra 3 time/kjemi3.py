# Chemical kinetics, also known as reaction kinetics, is the branch of physical chemistry that is concerned
# with understanding the rates of chemical reactions. It is to be contrasted with thermodynamics, which deals
# with the direction in which a process occurs but in itself tells nothing about its rate. Chemical kinetics
# includes investigations of how experimental conditions influence the speed of a chemical reaction and
# yield information about the reaction's mechanism and transition states, as well as the construction of
# mathematical models that also can describe the characteristics of a chemical reaction.


from chempy import ReactionSystem  # The rate constants below are arbitrary
rsys = ReactionSystem.from_string("""2 Fe+2 + H2O2 -> 2 Fe+3 + 2 OH-; 42
2 Fe+3 + H2O2 -> 2 Fe+2 + O2 + 2 H+; 17
H+ + OH- -> H2O; 1e10
H2O -> H+ + OH-; 1e-4""")  # "[H2O]" = 1.0 (actually 55.4 at RT)
print("""2 Fe+2 + H2O2 -> 2 Fe+3 + 2 OH-; 42
2 Fe+3 + H2O2 -> 2 Fe+2 + O2 + 2 H+; 17
H+ + OH- -> H2O; 1e10
H2O -> H+ + OH-; 1e-4""")
from chempy.kinetics.ode import get_odesys
odesys, extra = get_odesys(rsys)
from collections import defaultdict
import numpy as np
tout = sorted(np.concatenate((np.linspace(0, 23), np.logspace(-8, 1))))
c0 = defaultdict(float, {'Fe+2': 0.05, 'H2O2': 0.1, 'H2O': 1.0, 'H+': 1e-2, 'OH-': 1e-12})
result = odesys.integrate(tout, c0, atol=1e-12, rtol=1e-14)
import matplotlib.pyplot as plt
fig, axes = plt.subplots(1, 2, figsize=(12, 5))
for ax in axes:
    result.plot(names=[k for k in rsys.substances if k != 'H2O'], ax=ax)
    ax.legend(loc='best', prop={'size': 9})
    ax.set_xlabel('Time')
    ax.set_ylabel('Concentration')
axes[1].set_ylim([1e-13, 1e-1])
axes[1].set_xscale('log')
axes[1].set_yscale('log')
fig.tight_layout()
plt.show()
fig.savefig('kinetics.png', dpi=72)